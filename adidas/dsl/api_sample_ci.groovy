
job("api-sample-ci") {
    logRotator{numToKeep(5)}

    parameters {
        booleanParam('_test_param', false)
    }


    multiscm {
        git {
            remote {
                url("https://github.com/vovchenko/apiSampleJava")
            }
            branch("master")
        }

    }
    triggers {
        scm('* * * * *')
    }

    wrappers {
        preBuildCleanup()
        colorizeOutput()
        timestamps()
    }

    steps {
        shell ('''#!/bin/bash -xe
docker build . -t qrnd/apisampletest
docker push qrnd/apisampletest
''')
    }
}
