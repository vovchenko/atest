input_ar = [-1, 5, 2, 6, 4, 4, 2]
input_ar = [1, 5, 2, 6, 4, 4, 2]

def splitAr(ar):
    ar_len = len(ar)
    if ar_len == 1:
        return ar
    left = ar[:(ar_len / 2)]
    right = ar[(ar_len / 2):]
    return do_sort(splitAr(left), splitAr(right))


def do_sort(ar1, ar2):
    result = []

    iLeft = 0
    iRight = 0

    while (iLeft < len(ar1) and iRight < len(ar2)):
        if ar1[iLeft] < ar2[iRight]:
            result.append(ar1[iLeft])
            iLeft += 1
        else:
            result.append(ar2[iRight])
            iRight += 1

    result.extend(ar1[iLeft:])
    result.extend(ar2[iRight:])

    return result


print splitAr(input_ar)
