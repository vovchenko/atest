def myfunc(n):
  return lambda a : a * n
mydoubler = myfunc(3)
print(mydoubler(11))

print [5 if i / 2 else 4 for i in range(0, 4)]

print "Binary search"

def binarySearch(ls,data):
  first = 0
  last = len(ls)-1
  done = False

  while first<=last and not done:
    print first, last
    mid = (first+last)/2
    if ls[mid] == data:\
      done = True
    else:
      if ls[mid] > data:
        last = last-1
      else:
        first = first+1
  return done

print(binarySearch([2,5,7,8,9,11,14,16],4))

