def nswaps(l1):
    l2 = sorted(l1)
    cnt = 0
    for i in range(len(l1)):
        if l1[i] != l2[i]:
            ind = l2.index(l1[i])
            l2[i], l2[ind] = l2[ind], l2[i]
            cnt += 1
    return cnt

print nswaps([4, 3, 1, 2])